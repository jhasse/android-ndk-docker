FROM registry.fedoraproject.org/fedora-minimal:41 AS build

RUN microdnf install -y unzip ncurses-compat-libs java-17-openjdk-devel file git bzip2 patch gcc tar

WORKDIR /opt

RUN curl --silent -O https://dl.google.com/android/repository/commandlinetools-linux-10406996_latest.zip
RUN unzip *.zip && mkdir -p sdk/cmdline-tools/ && mv cmdline-tools/ sdk/cmdline-tools/latest
ENV JAVA_HOME=/usr
RUN yes | sdk/cmdline-tools/latest/bin/sdkmanager "build-tools;35.0.0" "platforms;android-34" \
                                                  "ndk;27.2.12479018" "platform-tools"

FROM registry.fedoraproject.org/fedora-minimal:41
COPY --from=build /opt/sdk /opt/sdk
RUN microdnf install -y java-17-openjdk-devel make git cmake ninja-build tar bzip2 patch gcc-c++ unzip && microdnf clean all
ENV ANDROID_SDK_ROOT=/opt/sdk
ENV LANG=C.utf8
ENV JAVA_HOME=/usr
RUN mkdir -p ~/.gradle/wrapper/dists/gradle-8.10.2-bin/a04bxjujx95o3nb99gddekhwo && \
    cd ~/.gradle/wrapper/dists/gradle-8.10.2-bin/a04bxjujx95o3nb99gddekhwo && \
    curl --silent -L -O https://services.gradle.org/distributions/gradle-8.10.2-bin.zip && \
    unzip *.zip && rm *.zip && touch gradle-8.10.2-bin.zip.lck gradle-8.10.2-bin.zip.ok && \
    mkdir -p ~/.gradle/notifications/8.10.2 && \
    touch ~/.gradle/notifications/8.10.2/release-features.rendered
